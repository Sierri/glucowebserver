package com.example.glucomedwebserver;

import com.example.glucomedwebserver.DTO.FoodDTO;
import com.example.glucomedwebserver.DTO.GlucoseMeasureDTO;
import com.example.glucomedwebserver.DTO.UserDTO;
import com.example.glucomedwebserver.jpaRepo.FoodRepo;
import com.example.glucomedwebserver.jpaRepo.GlucoseMeasureRepo;
import com.example.glucomedwebserver.jpaRepo.UserRepo;
import com.example.glucomedwebserver.jpaEntity.Food;
import com.example.glucomedwebserver.jpaEntity.GlucoseMeasure;
import com.example.glucomedwebserver.jpaEntity.User;
import com.example.glucomedwebserver.manager.FoodManager;
import com.example.glucomedwebserver.manager.GlucoseMeasureManager;
import com.example.glucomedwebserver.manager.UserManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.time.LocalDateTime;

@SpringBootApplication
public class GlucomedWebserverApplication {

    private GlucoseMeasureRepo glucoseMeasureRepo;
    private UserRepo userRepo;
    private FoodRepo foodRepo;
    private UserManager userManager;
    private FoodManager foodManager;
    private GlucoseMeasureManager glucoseMeasureManager;

    public GlucomedWebserverApplication(GlucoseMeasureRepo glucoseMeasureRepo, UserRepo userRepo, FoodRepo foodRepo) {
        this.glucoseMeasureRepo = glucoseMeasureRepo;
        this.userRepo = userRepo;
        this.foodRepo = foodRepo;
        userManager = new UserManager(this.userRepo);
        foodManager = new FoodManager(this.foodRepo, this.userRepo);
        glucoseMeasureManager = new GlucoseMeasureManager(this.glucoseMeasureRepo, this.userRepo);
    }

    public static void main(String[] args) {
        SpringApplication.run(GlucomedWebserverApplication.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB(){



        UserDTO u1=new UserDTO("marek123","potwor");
        UserDTO u2=new UserDTO("surykatka","guziec");
        UserDTO u3=new UserDTO("wolf","niemahasła");

        GlucoseMeasureDTO g1=new GlucoseMeasureDTO(u1,128.3, LocalDateTime.now());
        GlucoseMeasureDTO g2=new GlucoseMeasureDTO(u3,145.3, LocalDateTime.of(2020,11,18,13,20));
        GlucoseMeasureDTO g3=new GlucoseMeasureDTO(u2,122.5, LocalDateTime.of(2020,11,18,10,20));

        FoodDTO f1=new FoodDTO(u1,"drink","coca-cola",500,"ml",LocalDateTime.of(2020,10,28,16,0));
        FoodDTO f2=new FoodDTO(u1,"food","cake",200,"g",LocalDateTime.of(2020,10,25,15,0));
        FoodDTO f3=new FoodDTO(u3,"drink","bear",500,"ml",LocalDateTime.of(2020,10,15,20,0));

        saveUser(u1);
        saveUser(u2);
        saveUser(u3);
        if(glucoseMeasureRepo.count()==0)
        { saveGlucoseMeasure(g1);
            saveGlucoseMeasure(g2);
            saveGlucoseMeasure(g3);}
        if(foodRepo.count()==0)
        {saveFood(f1);
            saveFood(f2);
            saveFood(f3);}
    }
    private void saveGlucoseMeasure(GlucoseMeasureDTO g) {
        int result = glucoseMeasureManager.saveGlucoseMeasure(g);

    }
    private void saveUser(UserDTO u) {
        int result = userManager.saveUser(u);

    }
    private void saveFood(FoodDTO f) {
        int result = foodManager.saveFood(f);

    }





}


