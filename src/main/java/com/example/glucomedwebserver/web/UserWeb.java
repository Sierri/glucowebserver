package com.example.glucomedwebserver.web;

import com.example.glucomedwebserver.DTO.UserDTO;
import com.example.glucomedwebserver.manager.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserWeb {

    private UserManager userManager;

    @Autowired
    public UserWeb(UserManager userManager) {
        this.userManager = userManager;
    }


    @GetMapping
    public UserDTO getByUsernameAndPassword(@RequestParam String username, String password){
        return userManager.find(username, password);
    }

    @PostMapping
    public int addUser(@RequestBody UserDTO userDTO){
        return userManager.saveUser(userDTO);
    }


    @GetMapping("/all")
    public List<UserDTO> getAll(){
        return userManager.findAll();
    }

    @DeleteMapping
    public int deleteUser(@RequestBody UserDTO userDTO){
        return userManager.deleteUser(userDTO);
    }

}
