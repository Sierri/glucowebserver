package com.example.glucomedwebserver.web;

import com.example.glucomedwebserver.DTO.GlucoseMeasureDTO;
import com.example.glucomedwebserver.DTO.UserDTO;
import com.example.glucomedwebserver.manager.GlucoseMeasureManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/glucose")
public class GlucoseMeasureWeb {

private GlucoseMeasureManager glucoseMeasures;

    @Autowired
    public GlucoseMeasureWeb(GlucoseMeasureManager glucoseMeasures) {
        this.glucoseMeasures = glucoseMeasures;
    }

    @GetMapping("/all")
    public List<GlucoseMeasureDTO> getAll(@RequestParam String username, String password){
        return glucoseMeasures.findAll(new UserDTO(username,password));
    }


    @PostMapping
    public int addMeasure(@RequestBody GlucoseMeasureDTO glucoseMeasureDTO){
        return glucoseMeasures.saveGlucoseMeasure(glucoseMeasureDTO);
    }


    @DeleteMapping
    public int deleteMeasure(@RequestBody GlucoseMeasureDTO glucoseMeasureDTO){
        return glucoseMeasures.delete(glucoseMeasureDTO);
    }
}
