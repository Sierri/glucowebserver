package com.example.glucomedwebserver.web;



import com.example.glucomedwebserver.DTO.FoodDTO;
import com.example.glucomedwebserver.DTO.UserDTO;
import com.example.glucomedwebserver.manager.FoodManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/food")
public class FoodWeb {

private FoodManager foodManager;


    @Autowired
    public FoodWeb(FoodManager foodManager) {
        this.foodManager = foodManager;
    }


    @GetMapping("/all")
    public List<FoodDTO> getAll(@RequestParam String username, String password){
        return foodManager.findAll(new UserDTO(username,password));
    }

    @PostMapping
    public int addFood(@RequestBody FoodDTO food){
        return foodManager.saveFood(food);
    }


    @DeleteMapping
    public int deleteFood(@RequestBody FoodDTO foodDTO){
        return foodManager.deleteFood(foodDTO);
    }

}
