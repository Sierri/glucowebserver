package com.example.glucomedwebserver.jpaEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Food {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long foodId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    private String type;

    private String name;

    private int quantity;

    private String unitOfQuntity;

    private LocalDateTime dateTime;

    public Food() {
    }

    public Food( User user, String type, String name, int quantity, String unitOfQuntity, LocalDateTime dateTime) {

        this.user = user;
        this.type = type;
        this.name = name;
        this.quantity = quantity;
        this.unitOfQuntity = unitOfQuntity;
        this.dateTime = dateTime;
    }

    public Long getId() {
        return foodId;
    }

    public void setId(Long foodId) {
        this.foodId = foodId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getUnitOfQuntity() {
        return unitOfQuntity;
    }

    public void setUnitOfQuntity(String unitOfQuntity) {
        this.unitOfQuntity = unitOfQuntity;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "Food{" +
                "foodId=" + foodId +
                ", user=" + user +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", quantity=" + quantity +
                ", unitOfQuntity='" + unitOfQuntity + '\'' +
                ", dateTime=" + dateTime +
                '}';
    }


}
