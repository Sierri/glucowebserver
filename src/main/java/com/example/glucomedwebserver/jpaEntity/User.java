package com.example.glucomedwebserver.jpaEntity;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class User {

    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id
    private Long user_id;

    private String username;

    private String password;



    public User() {
    }

    public User( String username, String password) {

        this.username = username;
        this.password = password;
    }


    @Override
    public String toString() {
        return "User{" +
                "user_id=" + user_id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "user",cascade = CascadeType.REMOVE)
    private Collection<GlucoseMeasure> glucoseMeasure;

    public Collection<GlucoseMeasure> getGlucoseMeasure() {
        return glucoseMeasure;
    }

    public void setGlucoseMeasure(Collection<GlucoseMeasure> glucoseMeasure) {
        this.glucoseMeasure = glucoseMeasure;
    }

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "user", cascade = CascadeType.REMOVE)
    private Collection<Food> food;

    public Collection<Food> getFood() {
        return food;
    }

    public void setFood(Collection<Food> food) {
        this.food = food;
    }
}
