package com.example.glucomedwebserver.jpaEntity;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
public class GlucoseMeasure {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long measureId;
    private Double measure;

    private LocalDateTime measureDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    public GlucoseMeasure() {
    }

    public GlucoseMeasure(Double measure, LocalDateTime measureDateTime, User user) {

        this.measure = measure;
        this.measureDateTime = measureDateTime;
        this.user = user;
    }

    public Long getMeasure_ID() {
        return measureId;
    }

    public void setMeasure_ID(Long measureId) {
        this.measureId = measureId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Double getMeasure() {
        return measure;
    }

    public void setMeasure(Double measure) {
        this.measure = measure;
    }

    public LocalDateTime getMeasureDateTime() {
        return measureDateTime;
    }

    public void setMeasureDateTime(LocalDateTime measureDateTime) {
        this.measureDateTime = measureDateTime;
    }
}
