package com.example.glucomedwebserver.manager;


import com.example.glucomedwebserver.DTO.UserDTO;
import com.example.glucomedwebserver.jpaRepo.UserRepo;
import com.example.glucomedwebserver.jpaEntity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;



@Service
public class UserManager {

    private UserRepo userRepo;

    @Autowired
    public UserManager(UserRepo userRepo) {
        this.userRepo = userRepo;
    }



    public List<UserDTO> findAll(){
        List<UserDTO> userDTOList=new ArrayList<>();
        Iterable<User> users=userRepo.findAll();

        for(User user : users){
            UserDTO u=new UserDTO(user.getUsername(),user.getPassword());
            userDTOList.add(u);
        }

        return userDTOList;
    }




    public User findUser(UserDTO userDTO){

        User user=userRepo.findByUsernameAndPassword(userDTO.getUsername(), userDTO.getPassword());

        if (user!=null) {
            return user;
        }else {

            return null;
        }

    }

    public int saveUser(UserDTO userDTO){
        User user=findUser(userDTO);

        if(user==null) {
            User u =new User(userDTO.getUsername(),userDTO.getPassword());
            userRepo.save(u);
            return 1;
        }else{

            return 2;
        }
    }

    public UserDTO find(String username,String password){
        UserDTO userDTO=new UserDTO(username,password);
        User user=findUser(userDTO);
        if(user!=null){
            return userDTO;
        }else

            return null;

    }



    public int deleteUser(UserDTO userDTO){
        User user=findUser(userDTO);
        if (user!=null){
            userRepo.delete(user);
            return 1;
        }else {

            return 2;
        }
    }


}
