package com.example.glucomedwebserver.manager;

import com.example.glucomedwebserver.DTO.FoodDTO;
import com.example.glucomedwebserver.DTO.GlucoseMeasureDTO;
import com.example.glucomedwebserver.DTO.UserDTO;
import com.example.glucomedwebserver.jpaEntity.GlucoseMeasure;
import com.example.glucomedwebserver.jpaRepo.FoodRepo;
import com.example.glucomedwebserver.jpaRepo.UserRepo;
import com.example.glucomedwebserver.jpaEntity.Food;
import com.example.glucomedwebserver.jpaEntity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FoodManager {

    private final FoodRepo foodRepo;
    private final UserRepo userRepo;

    @Autowired
    public FoodManager(FoodRepo foodRepo, UserRepo userRepo) {
        this.foodRepo = foodRepo;
        this.userRepo = userRepo;
    }


    public List<FoodDTO> findAll(UserDTO userDTO) {
        User user = userRepo.findByUsernameAndPassword(userDTO.getUsername(),userDTO.getPassword());
        if(user!=null){
            List<Food> foods=foodRepo.findAllByUser(user);
            List<FoodDTO> foodDTOS=new ArrayList<>();
            for(Food food: foods){
                FoodDTO f =new FoodDTO(userDTO,food.getType(),food.getName(),food.getQuantity()
                        ,food.getUnitOfQuntity(),food.getDateTime());
                foodDTOS.add(f);
            }
            return foodDTOS;
        }else {

            return null;

        }
    }



    User user=new User();
    public Food findFood(FoodDTO foodDTO){
        user=userRepo.findByUsernameAndPassword(foodDTO.getUserDTO().getUsername(),foodDTO.getUserDTO().getPassword());
        if(user!=null){
            Food food=foodRepo.findByUserAndNameAndDateTime(user,foodDTO.getName(),foodDTO.getDateTime());
            return food;
        }
        return null;

    }

    public int saveFood(FoodDTO foodDTO){

        Food food= findFood(foodDTO);
        if(user!=null){
            if (food==null){
                food= new Food(user,foodDTO.getType(),foodDTO.getName()
                        ,foodDTO.getQuantity(),foodDTO.getUnitOfQuntity(),foodDTO.getDateTime());
                foodRepo.save(food);
                return 1;
            }else {
                return 2;

            }
        }else {
            return 3;

        }

    }


    public int deleteFood(FoodDTO foodDTO){
Food food =findFood(foodDTO);
if(user!=null){
    if(food!=null){
        foodRepo.delete(food);
        return 1;
    }else {

        return 2;
    }
}else {

    return 3;
}}


}
