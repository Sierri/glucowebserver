package com.example.glucomedwebserver.manager;

import com.example.glucomedwebserver.DTO.FoodDTO;
import com.example.glucomedwebserver.DTO.GlucoseMeasureDTO;
import com.example.glucomedwebserver.DTO.UserDTO;
import com.example.glucomedwebserver.jpaEntity.Food;
import com.example.glucomedwebserver.jpaRepo.GlucoseMeasureRepo;
import com.example.glucomedwebserver.jpaRepo.UserRepo;
import com.example.glucomedwebserver.jpaEntity.GlucoseMeasure;
import com.example.glucomedwebserver.jpaEntity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class GlucoseMeasureManager {

    private GlucoseMeasureRepo glucoseMeasureRepo;
    private UserRepo userRepo;

    @Autowired
    public GlucoseMeasureManager(GlucoseMeasureRepo glucoseMeasureRepo, UserRepo userRepo) {
        this.glucoseMeasureRepo = glucoseMeasureRepo;
        this.userRepo = userRepo;
    }


    public List<GlucoseMeasureDTO> findAll(UserDTO userDTO) {
        User user = userRepo.findByUsernameAndPassword(userDTO.getUsername(), userDTO.getPassword());
        if (user != null) {
            List<GlucoseMeasure> glucoseMeasures = glucoseMeasureRepo.findAllByUser(user);
            List<GlucoseMeasureDTO> glucoseDTOList = new ArrayList<>();
            for (GlucoseMeasure glucoseMeasure : glucoseMeasures) {
                GlucoseMeasureDTO g = new GlucoseMeasureDTO(userDTO, glucoseMeasure.getMeasure()
                        , glucoseMeasure.getMeasureDateTime());
                glucoseDTOList.add(g);
            }
            return glucoseDTOList;
        } else {

            return null;
        }
    }

    User user = new User();

    public GlucoseMeasure findGlucoseMeasure(GlucoseMeasureDTO glucoseMeasureDTO) {
        user = userRepo.findByUsernameAndPassword(glucoseMeasureDTO.getUserDTO().getUsername(),
                glucoseMeasureDTO.getUserDTO().getPassword());
        if (user != null) {
            GlucoseMeasure glucoseMeasure = glucoseMeasureRepo.findByUserAndMeasureAndMeasureDateTime(user
                    , glucoseMeasureDTO.getMeasure(), glucoseMeasureDTO.getMeasureDateTime());
            return glucoseMeasure;
        }
        return null;
    }

    public int saveGlucoseMeasure(GlucoseMeasureDTO glucoseMeasureDTO) {

        GlucoseMeasure glucoseMeasure = findGlucoseMeasure(glucoseMeasureDTO);
        if (user != null) {
            if (glucoseMeasure == null) {
                glucoseMeasure = new GlucoseMeasure(glucoseMeasureDTO.getMeasure()
                        , glucoseMeasureDTO.getMeasureDateTime(), user);
                glucoseMeasureRepo.save(glucoseMeasure);
                return 1;
            } else{

                return 2;
            }
        } else{

            return 3;
        }

    }



    public int delete(GlucoseMeasureDTO glucoseMeasureDTO) {

       GlucoseMeasure glucoseMeasure=findGlucoseMeasure(glucoseMeasureDTO);
       if(user!=null){
           if (glucoseMeasure!=null){

               glucoseMeasureRepo.delete(glucoseMeasure);
               return 1;

           }else{

               return 2;
           }
       }else {

           return 3;
       }


    }

}
