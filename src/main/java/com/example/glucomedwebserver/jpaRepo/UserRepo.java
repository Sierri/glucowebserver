package com.example.glucomedwebserver.jpaRepo;

import com.example.glucomedwebserver.jpaEntity.Food;
import com.example.glucomedwebserver.jpaEntity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepo extends CrudRepository<User,Long> {
    User findByUsernameAndPassword(String username,String password);

}
