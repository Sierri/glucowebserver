package com.example.glucomedwebserver.jpaRepo;

import com.example.glucomedwebserver.jpaEntity.GlucoseMeasure;
import com.example.glucomedwebserver.jpaEntity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface GlucoseMeasureRepo extends CrudRepository<GlucoseMeasure,Long> {
    List<GlucoseMeasure> findAllByUser(User user);
    GlucoseMeasure findByUserAndMeasureAndMeasureDateTime(User user, Double measure, LocalDateTime measureDateTime);

}
