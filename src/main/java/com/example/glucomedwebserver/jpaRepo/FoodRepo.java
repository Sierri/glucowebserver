package com.example.glucomedwebserver.jpaRepo;

import com.example.glucomedwebserver.jpaEntity.Food;
import com.example.glucomedwebserver.jpaEntity.User;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface FoodRepo extends CrudRepository<Food,Long> {
    List<Food> findAllByUser(User user);
    Food findByUserAndNameAndDateTime(User user, String name, LocalDateTime dateTime);
    Boolean existsByUserAndNameAndDateTime(User user, String name, LocalDateTime dateTime);
}
